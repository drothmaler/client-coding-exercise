import userDetailHtml from './userDetails.html'

/* @ngInject */
function UserDetailController (userService, $stateParams, $sce) {
  const vm = this
  vm.user = {}
  vm.mapsUrl = ''

  userService.user($stateParams.userId)
    .then((user) => {
      vm.user = user.data
      vm.mapsUrl = $sce.trustAsResourceUrl(
        // eslint-disable-next-line prefer-template
        'https://www.google.com/maps/embed/v1/place?key=AIzaSyAzhVgwz--S1zG3gzZsId9Hf2x1O3jhcuo&q=' +
        vm.user.address.geo.lng + ',' +
        vm.user.address.geo.lat +
        '&zoom=5')
    })
}

let userDetailComponent = {
  template: userDetailHtml,
  controller: UserDetailController
}

/* @ngInject */
export default userDetailComponent
