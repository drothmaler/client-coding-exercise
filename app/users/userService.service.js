/* @ngInject */
function userService ($http) {
  const baseUrl = 'http://jsonplaceholder.typicode.com'

  return {
    users: () => $http.get(`${baseUrl}/users/`),
    user: (id) => $http.get(`${baseUrl}/users/${id}`)
  }
}

export default userService
