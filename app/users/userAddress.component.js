import userAddressHtml from './userAddress.html'

angular
  .module('users')
  .component('userAddress', {
    bindings: {
      address: '='
    },
    template: userAddressHtml
  })
