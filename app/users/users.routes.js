function usersRoutes ($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.when('', '/users')
  $urlRouterProvider.when('/', '/users')
  $stateProvider
    .state('user-list', {
      url: '/users',
      component: 'userList'
    })
    .state('user-details', {
      url: '/users/:userId',
      component: 'userDetails'
    })
}

/* @ngInject */
export default usersRoutes
