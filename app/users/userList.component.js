import userListHtml from './userList.html'

/* @ngInject */
function UserListController (userService) {
  const vm = this
  vm.title = 'User List'
  vm.users = []

  userService.users()
    .then((users) => {
      vm.users = users.data
    })
}

let userListComponent = {
  template: userListHtml,
  controller: UserListController
}

/* @ngInject */
export default userListComponent
