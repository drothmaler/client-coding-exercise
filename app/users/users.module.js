import angular from 'angular'
import usersRoutes from './users.routes.js'
import userListComponent from './userList.component.js'
import userDetailsComponent from './userDetails.component.js'
import userService from './userService.service.js'

/* @ngInject */
angular
  .module('users', [])
  .component('userList', userListComponent)
  .component('userDetails', userDetailsComponent)
  .factory('userService', userService)
  .config(usersRoutes)
