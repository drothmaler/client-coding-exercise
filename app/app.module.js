import angular from 'angular'
import uiRouter from '@uirouter/angularjs'

import './users/users.module'

import './stylesheets/main.scss'

angular.module('app', [
  'jm.i18next',
  uiRouter,
  'users'
])
