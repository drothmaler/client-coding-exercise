module.exports = {
  scripts: {
    default: 'webpack-dev-server --config webpack.config.dev.babel.js --port 9000 --inline --hot',
    test: {
      default: 'echo "Error: no test specified" && exit 1'
    },
    build: {
      default: 'webpack --config webpack.config.dev.babel.js --progress --profile --colors',
      prod: 'webpack --config webpack.config.prod.babel.js -p --progress --profile --colors'
    }
  }
}
