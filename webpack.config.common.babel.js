import 'webpack'
import Config from 'webpack-config'
import path from 'path'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import CleanWebpackPlugin from 'clean-webpack-plugin'
import StyleLintPlugin from 'stylelint-webpack-plugin'

module.exports = new Config().merge({
  output: {
    path: path.join(__dirname, '/dist'),
    filename: 'bundle.js'
  },
  context: path.join(__dirname, '/app'),
  module: {
    rules: [{
      enforce: 'pre',
      test: /\.js$/,
      exclude: /(node_modules)/,
      use: [{
        loader: 'eslint-loader'
      }, {
        loader: 'standard-loader'
      }]
    }, {
      test: /\.scss$/,
      use: [{
        loader: 'style-loader'
      }, {
        loader: 'css-loader',
        options: {
          sourceMap: true
        }
      }, {
        loader: 'postcss-loader',
        options: {
          ident: 'postcss',
          plugins: (loader) => [
            // require('postcss-import')({ root: loader.resourcePath }),
            // require('postcss-cssnext')(),
            require('autoprefixer')(),
            require('cssnano')()
          ],
          sourceMap: true
        }
      }, {
        loader: 'sass-loader',
        options: {
          sourceMap: true
        }
      }]
    }, {
      test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
      use: {
        loader: 'file-loader',
        options: {
          mimetype: 'image/svg+xml'
        }
      }
    }, {
      test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
      use: {
        loader: 'file-loader',
        options: {
          mimetype: 'application/font-woff'
        }
      }
    }, {
      test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
      use: {
        loader: 'file-loader',
        options: {
          mimetype: 'application/font-woff'
        }
      }
    }, {
      test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
      use: {
        loader: 'file-loader',
        options: {
          mimetype: 'application/octet-stream'
        }
      }
    }, {
      test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
      use: {
        loader: 'file-loader'
      }
    }, {
      test: /\.json$/,
      use: {
        loader: 'json-loader'
      }
    }, {
      test: /\.html$/,
      use: {
        loader: 'ng-cache-loader',
        options: {
          prefix: '[dir]/[dir]'
        }
      }
    }, {
      test: /\.js$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['env'],
          plugins: [require('babel-plugin-angularjs-annotate')]
        }
      }
    }]
  },
  plugins: [
    new StyleLintPlugin({}),
    new CleanWebpackPlugin(['dist'], {
      root: __dirname,
      verbose: true,
      dry: false
    }),
    new HtmlWebpackPlugin({
      title: 'Client Coding Exercise',
      template: 'index.ejs',
      inject: 'body'
    })
  ]
})
