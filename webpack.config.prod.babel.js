import webpack from 'webpack'
import path from 'path'
import Config from 'webpack-config'

module.exports = new Config().extend('./webpack.config.common.babel.js').merge({
  entry: path.join(__dirname, '/app/app.module.js'),
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(true),
    new webpack.optimize.AggressiveMergingPlugin(),
    new webpack.optimize.UglifyJsPlugin()
  ]
})
