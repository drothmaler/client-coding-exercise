import webpack from 'webpack'
import path from 'path'
import Config from 'webpack-config'

module.exports = new Config().extend('./webpack.config.common.babel.js').merge({
  output: {
    pathinfo: true,
    filename: '[name].entry.js'
  },
  devtool: '#eval',
  entry: {
    bundle: path.join(__dirname, '/app/app.module.js'),
    vendor: ['angular', '@uirouter/angularjs']
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({ name: 'vendor', filename: 'vendor.js' }),
    new webpack.LoaderOptionsPlugin({
      debug: true
    })
  ]
})
